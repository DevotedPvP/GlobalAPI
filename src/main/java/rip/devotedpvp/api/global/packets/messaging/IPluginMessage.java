package rip.devotedpvp.api.global.packets.messaging;


public interface IPluginMessage {
    byte[] getBytes();

    void process(byte[] bytes);

    MessageType getType();
}
