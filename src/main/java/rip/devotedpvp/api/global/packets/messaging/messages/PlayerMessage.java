package rip.devotedpvp.api.global.packets.messaging.messages;

import rip.devotedpvp.api.global.packets.messaging.IPluginMessage;


public interface PlayerMessage extends IPluginMessage {
    String getName();
}
