package rip.devotedpvp.api.global.packets.messaging.messages.request;

import com.google.common.io.ByteArrayDataInput;
import rip.devotedpvp.api.global.packets.messaging.AbstractMessageObject;

import java.io.DataOutputStream;
import java.io.IOException;

public class PlayerDataRequest extends AbstractMessageObject {
    private String name;

    public PlayerDataRequest(String name) {
        super();
        this.name = name;
    }

    public PlayerDataRequest(byte[] bytes) {
        super(bytes);
    }


    public void serialize(ByteArrayDataInput in) {
        this.name = in.readUTF();
    }

    public void parse(DataOutputStream out) throws IOException {
        out.writeUTF(name);
    }

    public String getName() {
        return name;
    }
}
