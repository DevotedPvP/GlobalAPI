package rip.devotedpvp.api.global.packets.messaging.messages;


import rip.devotedpvp.api.global.packets.messaging.IPluginMessage;

import java.util.Map;

public interface DataMessage extends IPluginMessage {
    Map<String, String> getData();
}
