package rip.devotedpvp.api.global.packets.messaging.messages.request;

import com.google.common.io.ByteArrayDataInput;
import rip.devotedpvp.api.global.packets.messaging.AbstractMessageObject;
import rip.devotedpvp.api.global.type.ServerType;

import java.io.DataOutputStream;
import java.io.IOException;

public class PlayerServerRequest extends AbstractMessageObject {
    private ServerType type;

    public PlayerServerRequest(ServerType type) {
        this.type = type;
    }

    public PlayerServerRequest(byte[] bytes) {
        super(bytes);
    }

    public void serialize(ByteArrayDataInput in) {
        type = ServerType.getType(in.readUTF());
    }

    public void parse(DataOutputStream out) throws IOException {
        out.writeUTF(type.getName());
    }

    public ServerType getServerType() {
        return type;
    }
}
