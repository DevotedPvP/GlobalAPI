package rip.devotedpvp.api.global.packets;

import rip.devotedpvp.api.global.packets.messaging.IPluginMessage;

public interface PacketListener {
    void onMessage(PacketInfo info, IPluginMessage message);

    void onConnect(PacketInfo info);

    void onDisconnect(PacketInfo info);
}
