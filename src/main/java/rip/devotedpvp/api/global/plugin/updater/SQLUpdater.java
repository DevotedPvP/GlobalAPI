package rip.devotedpvp.api.global.plugin.updater;

import rip.devotedpvp.api.global.sql.SQLConnection;

import java.sql.SQLException;

public interface SQLUpdater {
    void update(SQLConnection connection) throws SQLException, ClassNotFoundException;

    String getName();
}
