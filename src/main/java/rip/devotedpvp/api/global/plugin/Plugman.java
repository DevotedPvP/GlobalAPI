package rip.devotedpvp.api.global.plugin;

import java.io.File;

public interface Plugman<DevotedPlugin> {
    void disable(DevotedPlugin p);

    void enable(DevotedPlugin p);

    void unload(DevotedPlugin p);

    DevotedPlugin load(File jar);

    DevotedPlugin get(String name);
}
